package number.pseudorandom.generator.dto;

import java.util.Set;

public class GenerateResponse {
    private Integer number;
    private Integer size;
    private Set<Integer> generated;

    public GenerateResponse() {}

    public GenerateResponse(Integer number, Integer size, Set<Integer> generated) {
        this.number = number;
        this.size = size;
        this.generated = generated;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Set<Integer> getGenerated() {
        return generated;
    }

    public void setGenerated(Set<Integer> generated) {
        this.generated = generated;
    }
}
