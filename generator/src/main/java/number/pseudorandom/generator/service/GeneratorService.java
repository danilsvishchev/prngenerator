package number.pseudorandom.generator.service;

import number.pseudorandom.generator.dto.GenerateResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

import static java.lang.System.*;

@Service
public class GeneratorService {
    private static final Logger logger = LoggerFactory.getLogger(GeneratorService.class);

    public GeneratorService() {}

    private Set<Integer> generateNumbers(int num) {
        Set<Integer> generatedNumbers = new HashSet<>();
        boolean generate = true;
        int number = num;
        while (generate) {
            number = ((number * number) / 100) % 10000;
            logger.info("Generated number: {}", number);
            generate = generatedNumbers.add(number);
        }
        logger.info("Generator was generated {} numbers", generatedNumbers.size());
        return generatedNumbers;
    }

    public GenerateResponse generate(int num) {
        Set<Integer> generateNumbers = generateNumbers(num);
        return new GenerateResponse(num, generateNumbers.size(), generateNumbers);
    }

    public GenerateResponse prnGenerator() {
        int counter = 9090;
        int numberOfMaxValue = 0;
        Set<Integer> maxGeneratedNumbers = new HashSet<>();
        while (counter > 99) {
            Set<Integer> generatedNumbers = generateNumbers(counter);
            if (generatedNumbers.size() > maxGeneratedNumbers.size()) {
                numberOfMaxValue = counter;
                maxGeneratedNumbers = new HashSet<>(generatedNumbers);
            }
            counter--;
        }
        logger.info("Generator generated maximum {} numbers by {}", maxGeneratedNumbers.size(), numberOfMaxValue);
        statistic(maxGeneratedNumbers);
        return new GenerateResponse(numberOfMaxValue, maxGeneratedNumbers.size(), maxGeneratedNumbers);
    }

    public void statistic (@NotNull Set<Integer> numbers) {
        int[] statistics = new int[10];
        for (Integer number: numbers) {
            int range = number / 1000;
            statistics[range] += 1;
        }
        logger.info("Generator statistic");
        for (int i = 0; i < statistics.length; i++) {
            out.print(i * 1000 + "-" + ((i + 1) * 1000 - 1) + ":\t");
            for (int j = 0; j < statistics[i]; j++) {
                out.print("=");
            }
            out.println("> " + statistics[i]);
        }
    }
}
