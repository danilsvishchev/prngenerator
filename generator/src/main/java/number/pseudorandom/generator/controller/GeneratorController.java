package number.pseudorandom.generator.controller;

import number.pseudorandom.generator.dto.GenerateResponse;
import number.pseudorandom.generator.service.GeneratorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/api/v1/generate")
public class GeneratorController {
    private final GeneratorService generatorService;

    public GeneratorController(GeneratorService generatorService) {
        this.generatorService = generatorService;
    }

    @GetMapping
    public GenerateResponse generateNumbers() {
        return generatorService.generate((int) (System.currentTimeMillis() % 10000));
    }

    @GetMapping("/byFixedNumber")
    public GenerateResponse generateNumbers(@RequestParam(name = "number") @Min(1) @Max(9999) Integer fixedNumber) {
        return generatorService.generate(fixedNumber);
    }

    @GetMapping("/findMax")
    public GenerateResponse findMaxGeneratedNumbers() {
        return generatorService.prnGenerator();
    }
}
